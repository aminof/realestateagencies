<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Property;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use App\Notification\ContactNotification;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;


/**
 * Description of PropertyController
 *
 * @author amine
 */
class PropertyController extends AbstractController {
    
    /**
     * @var PropertyRepository
     */
    private $repository;

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * class construct
     */
    public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
    {
        // We used the dependency injection (autowiring)
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/property/list", name="property_index")
     * @return Response
     */
    public function index(PaginatorInterface $paginator, Request $request) : Response
    {
        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        //$properties = $this->repository->findAllVisible();
        $properties = $paginator->paginate(
            $this->repository->findAllVisibleQuery($search), 
            $request->query->getInt('page', 1),
            12
        );
        return $this->render('property/index.html.twig', [
            'current_menu' => "property",
            'properties' => $properties,
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/property/{slug}-{id}", name="property.show", requirements={"slug"="[a-z0-9\-]*"})
     * @param Property $property
     * @param string $slug
     * @return Response
     */
    public function show(Property $property, string $slug, Request $request, ContactNotification $notification): Response
    {
         if($property->getSlug() !== $slug){
            return $this->redirectToRoute("property.show", [
                 'id' => $property->getId(),
                 'slug' => $property->getSlug()
             ], 301);
         }

        $contact = new Contact();
        $contact->setProperty($property);
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $notification->notify($contact);
            $this->addFlash('success', "Your email  was successfully sent");
            return $this->redirectToRoute("property.show", [
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ]);
        }

        return $this->render('property/show.html.twig', [
            'current_menu' => "properties",
            'property' => $property,
            'form' => $form->createView()
        ]);
    }
}
